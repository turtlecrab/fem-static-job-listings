# Frontend Mentor - Job listings with filtering

![Design preview for the Job listings with filtering coding challenge](./design/desktop-preview.jpg)

## Links

- Live site: <https://fem-static-job-listings-mu.vercel.app/>
- Solution page: <https://www.frontendmentor.io/solutions/job-listings-w-react-typescript-css-modules-mobile-first-9AD4-KjJVS>

## Notes

Used css modules and useCallback for the first time. The latter is added just for fun and practice, it would have worked just fine without it.
