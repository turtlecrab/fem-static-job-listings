import { useCallback, useState } from 'react'

import Header from './components/Header'
import TagList from './components/TagList'
import JobList from './components/JobList'
import Attribution from './components/Attribution'

import data from './data.json'

import st from './App.module.scss'

function App() {
  const [tags, setTags] = useState<string[]>([])

  const addTag = useCallback(
    (name: string) => {
      if (!tags.includes(name)) setTags([...tags, name])
    },
    [tags]
  )

  const removeTag = useCallback(
    (name: string) => {
      setTags(tags.filter((tag) => tag !== name))
    },
    [tags]
  )

  const filteredData = tags.length
    ? data.filter((job) => {
        return tags.every((tag) => {
          return (
            job.languages.includes(tag) ||
            job.tools.includes(tag) ||
            job.role === tag ||
            job.level === tag
          )
        })
      })
    : data

  return (
    <>
      <Header />
      <main className={st.main}>
        <TagList tags={tags} setTags={setTags} removeTag={removeTag} />
        <JobList data={filteredData} addTag={addTag} />
      </main>
      <Attribution />
    </>
  )
}

export default App
