import JobCard, { JobProps } from './JobCard'

import st from './JobList.module.scss'

interface Props {
  data: JobProps[]
  addTag: (name: string) => void
}

function JobList({ data, addTag }: Props) {
  return (
    <div className={st.jobList}>
      {data.map((job) => (
        <JobCard job={job} addTag={addTag} key={job.id} />
      ))}
    </div>
  )
}

export default JobList
