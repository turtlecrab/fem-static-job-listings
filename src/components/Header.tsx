import st from './Header.module.scss'

function Header() {
  return (
    <header className={st.header}>
      <h1 className={st.srOnly}>Job Listings</h1>
    </header>
  )
}

export default Header
