import TagFilter from './TagFilter'

import st from './TagList.module.scss'

interface Props {
  tags: string[]
  setTags: React.Dispatch<React.SetStateAction<string[]>>
  removeTag: (name: string) => void
}

function TagList({ tags, setTags, removeTag }: Props) {
  if (tags.length === 0) return null

  return (
    <div className={st.container}>
      <ul className={st.tagList}>
        {tags.map((tag) => (
          <TagFilter name={tag} removeTag={removeTag} key={tag} />
        ))}
      </ul>
      <button onClick={() => setTags([])} className={st.clearButton}>Clear</button>
    </div>
  )
}

export default TagList
