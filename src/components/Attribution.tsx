import st from './Attribution.module.scss'

function Attribution() {
  return (
    <footer className={st.attribution}>
      Challenge by{' '}
      <a
        href="https://www.frontendmentor.io?ref=challenge"
        target="_blank"
        rel="noreferrer"
      >
        Frontend Mentor
      </a>
      . Coded by{' '}
      <a
        href="https://www.frontendmentor.io/profile/turtlecrab"
        target="_blank"
        rel="noreferrer"
      >
        Turtle Crab
      </a>
      .
    </footer>
  )
}

export default Attribution
