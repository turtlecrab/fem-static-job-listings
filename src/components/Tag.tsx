import st from './Tag.module.scss'

interface Props {
  name: string
  addTag: (name: string) => void
}

function Tag({ name, addTag }: Props) {
  return (
    <li onClick={() => addTag(name)} className={st.tag}>
      {name}
    </li>
  )
}

export default Tag
