import Tag from './Tag'

import st from './JobCard.module.scss'

export interface JobProps {
  id: number
  company: string
  logo: string
  new: boolean
  featured: boolean
  position: string
  role: string
  level: string
  postedAt: string
  contract: string
  location: string
  languages: string[]
  tools: string[]
}

interface Props {
  job: JobProps
  addTag: (name: string) => void
}

function JobCard({ job, addTag }: Props) {
  return (
    <article className={st.card}>
      {job.featured && <div className={st.newMarker}></div>}
      <img
        className={st.logo}
        src={job.logo}
        alt={`${job.company} logo`}
      />
      <div className={st.info}>
        <div className={st.top}>
          <div className={st.company}>{job.company}</div>
          {job.new && <div className={st.flag}>New!</div>}
          {job.featured && <div className={st.flag + ' ' + st.featured}>Featured</div>}
        </div>
        <h2 className={st.position}>
          <a href={`./${job.id}`} onClick={(e) => e.preventDefault()}>
            {job.position}
          </a>
        </h2>
        <div className={st.details}>
          {job.postedAt} <span className={st.bullet}>•</span> {job.contract}{' '}
          <span className={st.bullet}>•</span> {job.location}
        </div>
        <hr className={st.hr} />
      </div>
      <ul className={st.tagList}>
        <Tag name={job.role} addTag={addTag} />
        <Tag name={job.level} addTag={addTag} />
        {job.languages.map((lang) => <Tag name={lang} key={lang} addTag={addTag} />)}
        {job.tools.map((tool) => <Tag name={tool} key={tool} addTag={addTag} />)}
      </ul>
    </article>
  )
}

export default JobCard
