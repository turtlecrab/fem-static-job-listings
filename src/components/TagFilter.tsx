import st from './TagFilter.module.scss'

interface Props {
  name: string
  removeTag: (name: string) => void
}

function Tag({ name, removeTag }: Props) {
  return (
    <li className={st.tag}>
      <div className={st.text}>{name}</div>
      <button
        onClick={() => removeTag(name)}
        className={st.closeButton}
        aria-label="Remove tag"
      >
        ✖
      </button>
    </li>
  )
}

export default Tag
